<form action="./post" method="POST" class="needs-validation" novalidate>

  <div class="mb-2">
    <span">
      <h3 style="display:inline">PRODUCT ADD</h3>
      <button type="reset" onclick="location.href='./list'" class="btn btn-danger mb-2 float-right" id="cancelButton">Cancel</button>
      <button type="submit" class="btn btn-primary mb-2 mr-1 float-right" id="submitButton">Submit</button>
    </span>
  </div>

  <div class="mb-3 mt-4">
    <label for="sku" class="form-label">SKU:</label>
    <input type="text" name="sku" class="form-control" id="sku" aria-describedby="skuHelp" required>
    <div class="invalid-feedback">Please enter unique SKU !</div>
    <div class="valid-feedback">OK !</div>
  </div>
  <div class="mb-3">
    <label for="name" class="form-label">Name:</label>
    <input type="text" name="name" class="form-control" id="name" aria-describedby="nameHelp" required>
    <div class="invalid-feedback">Please enter name !</div>
    <div class="valid-feedback">OK !</div>
  </div>
  <div class="mb-3">
    <label for="price" class="form-label">Price:</label>
    <input type="text" name="price" class="form-control" id="price" aria-describedby="priceHelp" required 
      pattern="^([1-9]{1}[0-9]{0,7}((\.{1}([0-9]{0,2}))|($))|[0]\.(([1-9][0-9]{0,1})|([0][1-9])))">
    <div class="invalid-feedback">Please enter price !</div>
    <div class="valid-feedback">OK !</div>
  </div>
  <div class="mb-3">
    <label for="type" class="form-label">Product type:</label><br>
    <select class="form-control product-type" name="type" id="type" aria-label="Product type select:" required>
      <option value="" selected disabled hidden>select product type</option>
      <option value="1">DVD</option>
      <option value="2">Book</option>
      <option value="3">Furniture</option>
      <option value="4">Weight + Dimension</option>
    </select>
    <div class="invalid-feedback">Please select product details type !</div>
    <div class="valid-feedback">OK !</div>
  </div>

  <div class="dynamic-container">

  </div>

  <!-- <button type="submit" class="btn btn-lg btn-primary mb-5" id="submitButton">Submit</button> -->
</form>

<script src="/js/config.js" defer></script>
<script src="/js/product/rules.js" defer></script>
<script src="/js/product/validation.js" defer></script>
<script src="/js/product/typeSelection.js" defer></script>
