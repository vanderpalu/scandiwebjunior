<form action="./delete" method="POST" name="mass_delete" novalidate>
    
    <div class="row">
        <div class="col">
            <h3 style="display:inline">PRODUCT LIST</h3>
        </div>
        <div class="col">
            <span>
                <button type="submit" class="btn btn-danger mb-2 float-right" id="cancelButton">Mass Delete</button>
                <button type="button" onclick="location.href='./add'" class="btn btn-primary mb-2 mr-1 float-right" id="submitButton">Add</button>
            </span>
        </div>
    </div>
    <div class="row">
        <?php
        if(!empty($params)){
            foreach ($params as $product) {
                ?>
                <div class="col-md-3">
                    <div class="card mb-2 shadow-sm">
                        <div class="card-body">
                            <input type="checkbox" name="<?= $product['id'] ?>" value="<?= $product['id'] ?>">
                            <h6 class="text-center"><?= $product['sku'] ?></h6>
                            <h6 class="text-center"><?= $product['name'] ?></h6>
                            <h6 class="text-center"><?= $product['price'] ?></h6>
                            <h6 class="text-center"><?= $product['primary_info'] ?></h6>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</form>