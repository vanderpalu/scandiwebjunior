<?php

namespace Scandiweb\controllers;

use Scandiweb\Application;
use Scandiweb\models\Api;

class ApiController extends Controller
{
    public function skuExists()
    {
        $request = Application::$app->request;
        $api = new Api();
        $response = $api->skuExists($request->getBody()["sku"]);
        header('Content-Type: application/text');
        echo $response;
    }
}
