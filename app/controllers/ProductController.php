<?php

namespace Scandiweb\controllers;

use Scandiweb\models\ProductModel;
use Scandiweb\Request;

class ProductController extends Controller
{
    public function index()
    {
        $productModel = new ProductModel();
        $products = $productModel->getAll();
        $this->createView('layouts/product', 'product/list', $products);
        return $this->view->render();
    }

    public function create()
    {
        $this->createView('layouts/product', 'product/add');
        return $this->view->render();
    }

    public function handlePost(Request $request)
    {
        $productModel = new ProductModel();
        $productModel->loadData($request->getBody());
        $productModel->loadProductRules();
        $productModel->loadPrimaryInfo();
        if($productModel->validate() && $productModel->save()){
            $this->redirect('/product/list');
        }else{
            $this->redirect('/product/add');
        }
    }

    public function handleDelete(Request $request)
    {
        $body = $request->getBody();
        if($this->isArrayOfNumbers($body)){
            $toBeDeleted = array_values($body);
            $productModel = new ProductModel();
            $productModel->delete($toBeDeleted);
        }

        $this->redirect('/product/list');
    }

    public function isArrayOfNumbers($arr)
    {
        if(!is_array($arr) || empty($arr)){
            return false;
        }
        foreach ($arr as $key => $value) {
            if(!is_numeric($value)){
                return false;
            }
        }

        return true;
    }
}
