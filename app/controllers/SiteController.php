<?php

namespace Scandiweb\controllers;

class SiteController extends Controller
{
    public function index()
    {
        $this->createView('layouts/main', 'home');
        return $this->view->render();
    }
}