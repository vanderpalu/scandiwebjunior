<?php

namespace Scandiweb\controllers;

use Scandiweb\View;
use Scandiweb\Application;
use Scandiweb\models\Model;

class Controller
{
    public Model $model;
    public View $view;

    public function createView(string $layout, string $content, array $params=[])
    {
        $this->view = new View($layout, $content, $params);
    }

    public function createModel(string $model)
    {
        # code...
    }

    public function redirect($redirectTo)
    {
        header("Location: {$_ENV['SERVER_PUBLIC_ROOT']}{$redirectTo}");
        exit();
    }
}
