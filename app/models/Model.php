<?php

namespace Scandiweb\models;

abstract class Model
{
    public array $errors = [];

    public function loadData($data)
    {
        foreach ($data as $key => $value) {
            if(property_exists($this, $key)){
                $this->{$key} = $value;
            }
        }
    }

    public function validate()
    {
        return true;
    }
}    