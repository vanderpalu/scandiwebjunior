<?php

namespace Scandiweb\models;

use PDO;
use PDOException;
use Scandiweb\Application;

class ProductModel extends Model
{
    public string $id;
    public string $sku;
    public string $name;
    public string $primary_info;
    public string $price;
    public string $type;
    public array $details;
    protected array $productDetailRules = [];

    public function rules()
    {
    }

    public function loadProductRules()
    {
        $this->productDetailRules = PRODUCT_TYPE_POLICY[$this->type];
    }

    public function validate()
    {
        return (parent::validate() && $this->validateDynamicDetails());
    }

    public function validateDynamicDetails()
    {
        $status = true;
        // Models can have different combinations of dynamic "groups of rules"
        // weight, size, weight+dimension, weight+size+dimension etc.
        foreach ($this->productDetailRules as $ruleGroupName => $group) {
            $rules = $this->productDetailRules[$ruleGroupName];

            // One "group" can have many rules
            foreach ($rules as $ruleName => $rule) {
                $productDetail = $this->details[$ruleGroupName][$ruleName];

                // One "rule" can have many attributes
                foreach ($rule as $attribute) {
                    // $rule is something like that: 'required' or ['min', 5]
                    if (!$this->validateAttribute($attribute, $ruleName, $productDetail)) {
                        $status = false;
                    }
                }
            }
        }

        return $status;
    }

    // $attributeName for error handling.
    public function validateAttribute($attribute, $attributeName, $productDetail)
    {
        $status = true;
        $ruleName = $attribute;
        if (!is_string($attribute)) {
            $ruleName = $attribute[0];
        }
        if ($ruleName === 'required' && !$productDetail) {
            echo "$attributeName is required!<br>";
            $status = false;
        }
        if ($ruleName === 'min' && strlen($productDetail) < $attribute[1]) {
            echo "$attributeName is too small!<br>";
            $status = false;
        }
        if ($ruleName === 'max' && strlen($productDetail) > $attribute[1]) {
            echo "$attributeName is too long!<br>";
            $status = false;
        }

        return $status;
    }

    public function save()
    {
        $db = Application::$app->db;

        try {
            $db->pdo->beginTransaction();
            $sku = $this->sku;
            $name = $this->name;
            $primary_info = $this->primary_info;
            $price = $this->price;
            $type = $this->type;

            $sql = "INSERT INTO `product` (`id`, `sku`, `name`, `primary_info`, `price`, `type_id`) 
            VALUES (NULL, :sku, :name, :primary_info, :price, :type);";
            $statement = $db->pdo->prepare($sql);
            
            $statement->execute([
                'sku' => $sku,
                'name' => $name,
                'primary_info' => $primary_info,
                'price' => $price,
                'type' => $type
            ]);

            $product_id = $db->pdo->lastInsertId();

            //Policy ["weight"=>[], "dimensions"=>[], "size_mb"=>[], ...]
            foreach ($this->productDetailRules as $name => $rule) {
                $tableName = "product_$name";
                $attributes[] = "product_id";
                $values["product_id"] = $product_id;
                //Field ["weight"] or ["height", "width", "length"], ...
                foreach ($this->details[$name] as $key => $value) {
                    $attributes[] = $key;
                    $values[$key] = $value;
                }

                $sqlAttributes = implode(',', $attributes);
                $sqlValues = implode(',', array_map(fn ($val) => ":$val", $attributes));

                $sql = "INSERT INTO $tableName ($sqlAttributes) 
                VALUES ($sqlValues);";
                $statement = $db->pdo->prepare($sql);

                foreach ($attributes as $key => $value) {
                    $statement->bindValue(":$value", $values[$value]);
                }
                $statement->execute();

                unset($attributes);
                unset($values);
            }
            $db->pdo->commit();
            return true;
        } catch (PDOException $e) {
            $db->pdo->rollBack();
            echo $e->getMessage().'<br>';
            return false;
        }
    }

    public static function getAll()
    {
        $arr = [];
        $db = Application::$app->db;
        try {
            $sql = "SELECT * FROM `product` ORDER BY id DESC";
            $statement = $db->pdo->prepare($sql);
            $statement->execute();
            $arr = $statement->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo $e->getMessage().'<br>';
        }finally{
            return $arr;
        }
    }

    public function getOne($id)
    {
        # code...
    }

    public function delete($ids)
    {
        $db = Application::$app->db;
        
        $placeholders = implode(',', array_fill(0, (count($ids)), '?'));
       
        try {
            $sql = "DELETE FROM `product` WHERE id IN ($placeholders);";
            $statement = $db->pdo->prepare($sql);
            foreach ($ids as $key => $value) {
                $statement->bindValue($key+1, (int)$value);
            }
            
            $statement->execute();
        } catch (PDOException $e) {
            echo $e->getMessage().'<br>';
        }
    }

    public function loadPrimaryInfo()
    {
        $this->primary_info = $this->createPrimaryInfo();
    }

    public function createPrimaryInfo()
    {
        //using Product type's 1-st rule policy as primary
        $name = array_key_first($this->productDetailRules);
        $noUnderscore = explode('_', $name);
        $value = ucfirst($noUnderscore[0]) . ': ';
        $value .= implode('x', $this->details[$name]);

        return $value;
    }
}
