<?php

namespace Scandiweb\models;

use Scandiweb\Application;

class Api
{
    public function skuExists($sku)
    {
        $db = Application::$app->db;
        $sql = "SELECT * FROM product
            WHERE sku = :sku;";
        $statement = $db->pdo->prepare("$sql");
        $statement->execute([
            'sku' => $sku
        ]);
        if(!$statement->fetchObject()){
            return json_encode(false);
        }

        return json_encode(true);
    }
}