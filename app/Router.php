<?php

namespace Scandiweb;

class Router
{
    private array $routes = [];
    public Request $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function get(string $path, $callback)
    {
        $this->routes['get'][$path] = $callback;
    }
    public function post(string $path, $callback)
    {
        $this->routes['post'][$path] = $callback;
    }

    public function resolve()
    {
        $requestPath = $this->request->getPath();
        $requestHttpMehtod = $this->request->getHttpMehod();
        $callback = $this->routes[$requestHttpMehtod][$requestPath] ?? false;

        if($callback === false){
            return '404';
        }
        if(is_string($callback)){
            return $callback;
        }
        if(is_array($callback)){
            $callback[0] = new $callback[0];
        }

        return call_user_func($callback, $this->request);
    }    
}