<?php

namespace Scandiweb;

class Application
{
    public static Application $app;
    public Request $request;
    public Router $router;
    public Database $db;

    public function __construct(array $config) 
    {
        self::$app = $this;
        $this->request = new Request();
        $this->router = new Router($this->request);
        $this->db = new Database($config['db']);
    }

    public function run()
    {
        echo $this->router->resolve();
    }
}