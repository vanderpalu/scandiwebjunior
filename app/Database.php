<?php 

namespace Scandiweb;

use PDO;

class Database
{
    public PDO $pdo;

    public function __construct(array $config) 
    {
        $dsn = $config['dsn'] ?? '';
        $user = $config['user'] ?? '';
        $password = $config['password'] ?? '';
        $this->pdo = new PDO($dsn, $user, $password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function applyMigrations()
    {
        $this->createMigrationsTable();
        $appliedMigrations = $this->getAppliedMigrations();

        $newMigrations = [];
        $files = scandir('./migrations');
        $toApply = array_diff($files, $appliedMigrations);
        foreach ($toApply as $migration) {
            if($migration === '.' || $migration === '..'){
                continue;
            }

            require_once './migrations/'.$migration;
            $className = pathinfo($migration, PATHINFO_FILENAME);
            $instance = new $className;
            $instance->up();
            echo "Applied migration $migration".PHP_EOL;
            $newMigrations[] = $migration;
        }

        if(!empty($newMigrations)){
            $this->saveMigrations($newMigrations);
        }else{
            echo 'All migrations are applied.'.PHP_EOL;
        }
    }

    public function createMigrationsTable()
    {
        $this->pdo->exec("CREATE TABLE IF NOT EXISTS migrations (
            id INT AUTO_INCREMENT,
            migration VARCHAR(255),
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY(id)
        ) ENGINE=INNODB;"); 
    }

    public function getAppliedMigrations()
    {
        $statement = $this->pdo->prepare("SELECT migration FROM migrations");
        $statement->execute();
 
        return $statement->fetchAll(PDO::FETCH_COLUMN);
    }

    public function saveMigrations(array $migrations)
    {
        $string = implode(",", array_map(fn($mig) => "('$mig')", $migrations));
        $statement = $this->pdo->prepare("INSERT INTO migrations (migration)
            VALUES $string");
        $statement->execute();    
    }
}