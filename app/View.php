<?php

namespace Scandiweb;

class View 
{
    protected $layout;
    protected $content;
    protected $params;

    public function __construct(string $layout, string $content, array $params=[])
    {
        $this->layout = $layout;
        $this->content = $content;
        $this->params = $params;
    }

    public function render()
    {
        $layout = $this->renderLayout($this->layout);
        $content = $this->renderContent($this->content, $this->params);
        return str_replace('{{content}}', $content, $layout);
    }

    public function renderLayout(string $layout)
    {
        ob_start();
        include_once dirname(__DIR__) . "/app/views/$layout.php";
        return ob_get_clean();
    }

    public function renderContent(string $content, array $params)
    {
        ob_start();
        include_once dirname(__DIR__) . "/app/views/$content.php";
        return ob_get_clean();
    }

}