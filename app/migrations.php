<?php
include_once('../vendor/autoload.php');

use Dotenv\Dotenv;
use Scandiweb\Application;
use Scandiweb\controllers\ProductController;
use Scandiweb\controllers\SiteController;

$dotenv = Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

$config = [
    'db' => [
        'dsn' => $_ENV['DB_DNS'],
        'user' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PASSWORD'],
    ]
];

$app = new Application($config);

$app->db->applyMigrations();