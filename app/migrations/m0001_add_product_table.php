<?php

use Scandiweb\Application;

// product, product_type, 
class m0001_add_product_table
{
    public function up()
    {
        $db = Application::$app->db;
        $db->pdo->exec("CREATE TABLE product(
            id INT AUTO_INCREMENT,
            sku VARCHAR(255) NOT NULL,
            name VARCHAR(255) NOT NULL,
            price DECIMAL(10,2) NOT NULL,
            type_id INT,
            primary_info VARCHAR(255),
            PRIMARY KEY(id),
            UNIQUE(sku)
        )  ENGINE=INNODB;");
    }

    public function down()
    {
        $db = Application::$app->db;
        $db->pdo->exec("DROP TABLE product;");
    }
} 