<?php

use Scandiweb\Application;

// product, product_type, 
class m0003_add_product_size_mb_table
{
    public function up()
    {
        $db = Application::$app->db;
        $db->pdo->exec("CREATE TABLE product_size_mb(
            id INT AUTO_INCREMENT,
            product_id INT NOT NULL,
            size INT NOT NULL,
            PRIMARY KEY(id),
            INDEX(product_id)
        );");

        $db->pdo->exec("ALTER TABLE `product_size_mb`
            ADD CONSTRAINT `FK_product_size_mb` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) 
            ON DELETE CASCADE ON UPDATE NO ACTION;
        ");
    }

    public function down()
    {
        $db = Application::$app->db;
        $db->pdo->exec("DROP TABLE product_size_mb;");
    }
} 