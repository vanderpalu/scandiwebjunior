<?php

use Scandiweb\Application;

// product, product_type, 
class m0002_add_product_weight_table
{
    public function up()
    {
        $db = Application::$app->db;
        $db->pdo->exec("CREATE TABLE product_weight(
            id INT AUTO_INCREMENT,
            product_id INT NOT NULL,
            weight DECIMAL(10,3) NOT NULL,
            PRIMARY KEY(id),
            INDEX(product_id)
        )");

        $db->pdo->exec("ALTER TABLE `product_weight`
            ADD CONSTRAINT `FK_product_weight` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) 
            ON DELETE CASCADE ON UPDATE NO ACTION;
        ");
    }

    public function down()
    {
        $db = Application::$app->db;
        $db->pdo->exec("DROP TABLE product_weight;");
    }
} 