<?php

use Scandiweb\Application;

class m0004_add_product_dimensions_table
{
    public function up()
    {
        $db = Application::$app->db;
        $db->pdo->exec("CREATE TABLE product_dimensions(
            id INT AUTO_INCREMENT,
            product_id INT NOT NULL,
            height INT NOT NULL,
            width INT NOT NULL,
            length INT NOT NULL,
            PRIMARY KEY(id),
            INDEX(product_id)
        )  ENGINE=INNODB;");

        $db->pdo->exec("ALTER TABLE `product_dimensions`
            ADD CONSTRAINT `FK_product_dimensions` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
            ON DELETE CASCADE ON UPDATE NO ACTION;
        ");
    }

    public function down()
    {
        $db = Application::$app->db;
        $db->pdo->exec("DROP TABLE product_dimensions;");
    }
} 