<?php
/**
 *  Policy names ("size_mb", "weight", "dimensions", ...) 
 * have relevant details tables sith "product_" prefix: 
 * ("product_size_mb", "product_weight", "product_dimensions", ...)
 * 
 *  "size", "weight", "height" are column names.
 * Form field names should be: details["size_mb"]["size"] etc.
 *  details["dimensions"]["height"] limits are the same in DB level,
 * but can be made different on a 
  */
define("PRODUCT_TYPE_POLICY", [
    "1" => ["size_mb" => [
        "size" => ['required', ['min', 0], ['max', 10]]
    ]], 
    "2" => ["weight" => [
        "weight" => ['required', ['min', 0], ['max', 10]]
    ]], 
    "3" => ["dimensions" => [
        "height" => ['required', ['min', 0], ['max', 5]],
        "width" => ['required', ['min', 0], ['max', 5]],
        "length" => ['required', ['min', 0], ['max', 5]],
    ]], 
    "4" => [
        "weight" => [
            "weight" => ['required', ['min', 0], ['max', 10]]
        ],
        "dimensions" => [
            "height" => ['required', ['min', 0], ['max', 5]],
            "width" => ['required', ['min', 0], ['max', 5]],
            "length" => ['required', ['min', 0], ['max', 5]],
        ],
    ], 
]);