<?php

namespace Scandiweb;

class Request
{
    public function getHttpMehod() : string
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    // Expected request format: "/foo/bar?id=23"
    public function getPath() : string
    {
        $path = $_SERVER['REQUEST_URI'] ?? '/';
        $position = strpos($path, '?');
        if($position === false){
            return $path;
        }

        return substr($path, 0, $position);
    }

    // Some input sanitation here
    public function getBody()
    {

        return $_POST;
    }
}
