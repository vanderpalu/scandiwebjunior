const COMPONENTS_PATH = 'http://localhost:8080/components/form/'

const POLICIES = {
    "1": ['size_mb'],
    "2": ['weight'],
    "3": ['dimensions'],
    "4": ['weight', 'dimensions']
}

const FIELD_NAMES = {
    "sku": "SKU",
    "name": "Name",
    "price": "Price",
    "type": "Product type",
    "size_mb": "Size",
    "weight": "Weight",
    "dimensions_height": "Height",
    "dimensions_width": "Width",
    "dimensions_length": "Length",
}

const REGEX_DESCRIPTION = {
    "price": "10.01",
    "weight": "10.001"
}