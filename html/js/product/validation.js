const form = document.querySelector('.needs-validation')

class SkuFetcher {
    constructor(sku) {
        this.sku = sku
    }

    async doesSkuExist() {
        try {
            let response = await fetch('/api/sku', {
                method:'POST',
                mode: 'same-origin',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body: `sku=${this.sku}`
                })
            return await response.json()
        } catch (err) {
            console.log(err);
        }
    }
}

class FormValidator {
    constructor(form) {
        this.form = form
        this.fields = []
    }

    init() {
        this.validateOnSubmit()
    }

    validateOnSubmit() {
        this.form.addEventListener("submit", e => {
            e.preventDefault()
            this.fields = this.loadFields()

            let errors = []
            this.fields.forEach((field) => {
                const currentField = document.getElementById(field.id)
                currentField.value = currentField.value.trim()
                let error = this.validateField(currentField)
                errors.push(error)
            });

            Promise.all(errors)
                .then(async (data)=>{
                    let errorsRemaining = data.filter(el => el.length !== 0)
                    // No Errors Remaining !
                    if(errorsRemaining.length === 0){
                        fetch(this.form.action, {
                            method: this.form.method,
                            redirect: 'follow',
                            body: new FormData(this.form)
                          }).then(response => {
                              console.log(response);
                            if (response.redirected) {
                                window.location.href = response.url;
                            }
                        }).catch(err => {
                            console.log(err);
                        })
                    }
                })
                .catch(err => {
                    console.log(err);
                })

        })
    }

    async validateField(field) {
        let errors = []

        if(field.id === 'sku'){
            if(field.value.length < 6){
                errors = `${FIELD_NAMES[field.id]} is too short !`
            }

            if(typeof errors === 'undefined' || errors.length === 0) {
                await (async () => {
                    const skuFetcher = new SkuFetcher(field.value)
                    let skuExists = await skuFetcher.doesSkuExist()
                    if(skuExists){
                        errors = `This ${FIELD_NAMES[field.id]} already exists !`
                    }
                })()
            }
        }else {
            if(!PRODUCT_FORM_RULES[field.id]){
                return
            }
            for (const [key, expected] of Object.entries(PRODUCT_FORM_RULES[field.id])) {
                switch (key) {
                    case 'minlength':
                        if(field.value.length < expected) {
                            errors = `${FIELD_NAMES[field.id]} has to be longer than ${expected}`
                        }
                        break;

                    case 'maxlength':
                        if(field.value.length > expected) {
                            errors = `${FIELD_NAMES[field.id]} has to be shorter than ${expected}`
                        }
                        break;

                    case 'minint':
                        if(field.value.trim().length === 0){
                            errors = `${FIELD_NAMES[field.id]} cannot be empty`
                            break
                        }
                        
                        if(field.value.trim() < expected) {
                            errors = `${FIELD_NAMES[field.id]} value has to be at least ${expected}`
                            break
                        }
                        
                        if (!/^\d+$/.test(field.value.trim())){
                            errors = `${FIELD_NAMES[field.id]} value has to be a number`
                            break
                        }
                        break;

                    case 'maxint':
                        if(field.value.trim() > expected) {
                            errors = `${FIELD_NAMES[field.id]} value is too big`
                            break
                        }
                        
                        if (!/^\d+$/.test(field.value.trim())){
                            errors = `${FIELD_NAMES[field.id]} value has to be a number`
                            break
                        }
                        break;

                    case 'regex':
                        if(!expected.test(field.value.trim())) {
                            errors = `${FIELD_NAMES[field.id]} expected format is: ${REGEX_DESCRIPTION[field.id]}`
                            break   
                        }
                        break;
                
                    default:
                        break;
                }

                // We display only the first error occurence on a field !
                if(errors.length > 0){
                    break
                }
             }
        }

        if(typeof errors !== 'undefined' && errors.length !== 0){
            this.setStatus(field, errors, 'is-invalid')
        }else {
            this.setStatus(field, '', 'is-valid')
        }

        return errors
    }
    
    setStatus(field, message, status) {
        if(status === 'is-valid'){
            field.classList.remove('is-invalid')
            field.classList.add('is-valid')
            let validityInfo = field.parentElement.querySelector('.valid-feedback')
            validityInfo.textContent = message
        }else if(status === 'is-invalid') {
            field.classList.remove('is-valid')
            field.classList.add('is-invalid')
            let validityInfo = field.parentElement.querySelector('.invalid-feedback')
            validityInfo.textContent = message
        }
    }

    loadFields() {
        const formFields = document.querySelectorAll('.form-control')
        let tmp = []
        formFields.forEach(field => {
            tmp.push(field)
        });
        return tmp
    }
}

const validator = new FormValidator(form)
validator.init()




