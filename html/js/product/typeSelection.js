const typeSelection = document.querySelector(".form-control.product-type")
const dynamicContainer = document.querySelector(".dynamic-container")

function getRequiredComponents(id){
    return POLICIES[id]
}

typeSelection.addEventListener('change', e => {
    // const dynamicContainer = document.querySelector(".dynamic-container")

    while (dynamicContainer.hasChildNodes()) {
        dynamicContainer.removeChild(dynamicContainer.lastChild);
    }
    
    names = getRequiredComponents(e.target.value)
    names.forEach(component => {
        let url = COMPONENTS_PATH + component + '.html'
        fetch(url)
            .then(res => {
                if(!res.ok){
                    throw Error('Error')
                }
                return res.text()
            }).then(data => {
                 dynamicContainer.insertAdjacentHTML("beforeend", data)
            }).catch(error =>{
                console.log('Fetch Error', error)
            })
        
    });
})

