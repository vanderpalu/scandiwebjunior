const PRODUCT_FORM_RULES = {
    sku: {
    },
    name: {
        minlength: 5
    },
    price: {
        regex: /^([1-9]{1}[0-9]{0,7}((\.{1}([0-9]{0,2}))|($))|[0]\.(([1-9][0-9]{0,1})|([0][1-9])))/
    },
    type: {
        minint: 1
    },
    size_mb: {
        minint: 1,
        maxint: 9999999999
    },
    weight: {
        regex: /^([1-9]{1}[0-9]{0,6}((\.{1}([0-9]{0,3}))|($))|[0]\.(([1-9][0-9]{0,2})|(([0][1-9][0-9])|([0][0][1-9])|([0][1-9]{1,2}))))/
    },
    dimensions_height: {
        minint: 1,
        maxint: 9999999999
    },
    dimensions_width: {
        minint: 1,
        maxint: 9999999999
    },
    dimensions_length: {
        minint: 1,
        maxint: 9999999999
    },
}
