<?php
include_once('../vendor/autoload.php');
include_once('../app/config/product_type_policies.php');

use Dotenv\Dotenv;
use Scandiweb\Application;
use Scandiweb\controllers\ApiController;
use Scandiweb\controllers\ProductController;
use Scandiweb\controllers\SiteController;

$dotenv = Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

$config = [
    'db' => [
        'dsn' => $_ENV['DB_DNS'],
        'user' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PASSWORD'],
    ]
];

$app = new Application($config);

$app->router->get('/home/index', [SiteController::class, 'index']);
$app->router->get('/product/list', [ProductController::class, 'index']);
$app->router->get('/product/add', [ProductController::class, 'create']);

$app->router->post('/product/post', [ProductController::class, 'handlePost']);
$app->router->post('/product/delete', [ProductController::class, 'handleDelete']);

$app->router->post('/api/sku', [ApiController::class, 'skuExists']);

$app->run();